import React, { useState } from 'react';
import { Button, FormControl, Grid, InputLabel, makeStyles, MenuItem, Paper, Select } from '@material-ui/core';
import { useLazyQuery, useQuery } from '@apollo/client';
import HOTEL_LIST from '../queries/GetHotels';

import { useHistory } from 'react-router';
import GetHotel from '../queries/GetHotel';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },
    formControl: {
        width: '100%'
    }
}));

function HotelLazyQuery() {

    let history = useHistory();
    const classes = useStyles();
    const [selectedHotel, setSelected] = useState('');

    const [getHotelByID, { data: hotelData}] = useLazyQuery(GetHotel);
    const { loading, error, data } = useQuery(HOTEL_LIST);

    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error :(</p>;


    const handleChange = (event) => {
        setSelected(event.target.value);
        getHotelByID({ variables: { id: +event.target.value } });
    };


    function goBack() {
        history.push("/");
    }

    const renderMenuItems = (data) => {
        if (data) {
            return data.hotels.map(({ id, name }) => {
                return <MenuItem key={id} value={id}>{name}</MenuItem>
            });
        }
    }

    const renderHotel = ({ name, address, rating }) => {
        return (
            <div>
                <h2>Hotel Details</h2>
                <Paper elevation={3} className={classes.paper}>
                    <h3 style={{ color: "blue" }}>{name}</h3>
                    <h4>{address}</h4>
                    <strong style={{ color: "green" }}>Rating:{rating}</strong>
                </Paper>
            </div>
        )
    }

    const renderHotelData = ({amenities, reviews}) => {
        return (
            <Grid item xs={12}>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <Paper elevation={3} className={classes.paper} spacing={3}>
                            <h3>Amenities</h3>
                            <p>Wifi :{amenities.wifi ? "Available" : "Not Available"}</p>
                            <p>SwimmingPool :{amenities.swimmingPool ? "Available" : "Not Available"}</p>
                        </Paper>
                    </Grid>
                    <Grid item xs={12}>
                        <Paper elevation={3} className={classes.paper}>
                            <h3>Reviewes</h3>
                            <div>{renderReviewes(reviews)}</div>
                        </Paper>
                    </Grid>
                </Grid>
            </Grid>
        )
    }

    const renderReviewes = (reviewes) => {
        if (!reviewes) {
            return <div></div>;
        }
        return reviewes.map(({ reviewer, comments }) => {
            return (
                <div key={reviewer}>
                    <p>Reviewer : {reviewer}</p>
                    <p>Comments : {comments}</p>
                </div>
            );
        });
    };

    return (
        <div className={classes.root}>
            <Grid container spacing={3}>
                <Grid item xs={12} >
                    <h2>Lazy Query Demo</h2>
                    <Paper elevation={3} className={classes.paper}>
                        <FormControl className={classes.formControl}>
                            <InputLabel shrink>Hotel</InputLabel>
                            <Select
                                value={selectedHotel}
                                onChange={handleChange}
                                displayEmpty
                            >
                                <MenuItem value="">
                                    <em>None</em>
                                </MenuItem>
                                {renderMenuItems(data)}
                            </Select>
                        </FormControl>
                    </Paper>
                </Grid>
                <Grid item xs={12} >
                    {hotelData && renderHotel(hotelData.hotel)}
                </Grid>
                {hotelData && renderHotelData(hotelData.hotel)}
                <Grid item xs={12}>
                    <Button variant="contained" color="secondary" onClick={goBack}>Go Back</Button>
                </Grid>
            </Grid>
        </div>
    )
}

export default HotelLazyQuery;
