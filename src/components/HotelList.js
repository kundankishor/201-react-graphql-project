import React from 'react';
import { useQuery } from '@apollo/client';
import { Link } from 'react-router-dom';
import HOTEL_LIST from '../queries/GetHotels';
import { Button, Grid, Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
}));

function HotelList() {
    const classes = useStyles();

    const { loading, error, data } = useQuery(HOTEL_LIST);

    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error :(</p>;

    return (
        <div className={classes.root}>
            <h2>Hotel List</h2>
            <Grid container spacing={1}>
                {
                    data.hotels.map(({ id, name, address, rating }) => (
                        <Grid item xs={12} key={id}>
                            <Link to={`/hotels/${id}`} style={{ textDecoration: "auto" }}>
                                <Paper elevation={3} className={classes.paper}>
                                    <div style={{ fontSize: "24px" }}>{name}</div>
                                    <div style={{ fontSize: "20px" }}>{address}</div>
                                    <div style={{ color: "green", fontSize: "22px" }}>{rating}</div>
                                </Paper>
                            </Link>
                        </Grid>
                    ))
                }
                <Grid container spacing={3}>
                    <Grid item xs={6}>
                        <Link to="/hotels/add">
                            <Button style={{ margin: "5px" }} variant="contained" color="primary" >Create Hotel</Button>
                        </Link>
                        <Link to="/lazyquery">
                            <Button style={{ margin: "5px" }} variant="contained" color="primary" >Demo Lazy Query</Button>
                        </Link>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    )
}

export default HotelList;
