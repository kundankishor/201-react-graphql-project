import React from 'react';
import { useQuery } from '@apollo/client';
import { useHistory, useParams } from 'react-router';
import HOTEL_DETAIL from '../queries/GetHotel';
import { Button, Grid, makeStyles, Paper } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },
}));

function HotelDetails() {
    const classes = useStyles();
    let history = useHistory();

    function handleClick() {
        history.push("/");
    }
    const renderReviewes = (reviewes) => {
        if (!reviewes) {
            return <div></div>;
        }
        return reviewes.map(({ reviewer, comments }) => {
            return (
                <div key={reviewer}>
                    <p>Reviewer : {reviewer}</p>
                    <p>Comments : {comments}</p>
                </div>
            );
        });
    };
    const { id } = useParams();
    const { loading, error, data } = useQuery(HOTEL_DETAIL, {
        variables: { id: +id }
    });

    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error :(</p>;
    const { name, address, rating, amenities, reviews } = data.hotel;
    return (
        <div className={classes.root}>
            <Grid container spacing={3}>
                <Grid item xs={12} key={id}>
                    <h2>Hotel Details</h2>
                    <Paper elevation={3} className={classes.paper}>
                        <h3 style={{ color: "blue" }}>{name}</h3>
                        <h4>{address}</h4>
                        <strong style={{ color: "green" }}>Rating:{rating}</strong>
                    </Paper>
                </Grid>
                <Grid item xs={12}>
                    <Grid container spacing={3}>
                        <Grid item xs={12}>
                            <Paper elevation={3} className={classes.paper} spacing={3}>
                                <h3>Amenities</h3>
                                <p>Wifi :{amenities.wifi ? "Available" : "Not Available"}</p>
                                <p>SwimmingPool :{amenities.swimmingPool ? "Available" : "Not Available"}</p>
                            </Paper>
                        </Grid>
                        <Grid item xs={12}>
                            <Paper elevation={3} className={classes.paper}>
                                <h3>Reviewes</h3>
                                <div>{renderReviewes(reviews)}</div>
                            </Paper>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <Button variant="contained" color="secondary" onClick={handleClick}>Go Back</Button>
                </Grid>
            </Grid>
        </div>
    )
}

export default HotelDetails;
