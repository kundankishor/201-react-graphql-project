import React, { useState } from 'react';
import { Button, FormGroup, Grid, makeStyles, Paper, TextField } from '@material-ui/core';
import { useMutation } from '@apollo/client';
import CREATE_HOTEL from '../mutations/CreateHotel';
import HOTEL_LIST from '../queries/GetHotels';
import { useHistory } from 'react-router';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },
}));

function CreateHotel() {

    const classes = useStyles();

    let history = useHistory();

    function goBack() {
        history.push("/");
    }

    const [addHotel, { error, data }] = useMutation(CREATE_HOTEL, {
        refetchQueries: [{ query: HOTEL_LIST }]
    });

    const [hotelName, setHotelName] = useState('');
    const [address, setAddress] = useState('');
    const [rating, setRating] = useState('');
    const [phone, setPhone] = useState('');
    const [email, setEmail] = useState('');
    const [country, setCountry] = useState('');
    const [pincode, setPincode] = useState('');
    const [latitude, setLatitude] = useState('');
    const [longitude, setlongitude] = useState('');
    const [checkIn, setCheckIn] = useState('');
    const [checkOut, setCheckOut] = useState('');

    const onChangeValue = (value, cb) => {
        cb(value);
    }

    const onSubmit = (event) => {
        event.preventDefault();
        addHotel({
            variables: {
                hotel: {
                    name: hotelName,
                    address,
                    rating: +rating,
                    phone,
                    email,
                    country,
                    pincode,
                    latitude: +latitude,
                    longitude: +longitude,
                    checkIn,
                    checkOut
                }
            }
        }).then(res => setTimeout(() => {
            goBack()
        }, 2000));
    }

    return (
        <div className={classes.root}>
            <Grid container spacing={3}>
                <Grid item xs={12} >
                    <Paper elevation={3} className={classes.paper}>
                        <form onSubmit={(event) => onSubmit(event)}>
                            <h2>Add new Hotel</h2>
                            <FormGroup>
                                <TextField id="standard-basic" required label="Hotel Name" value={hotelName} onChange={(event) => onChangeValue(event.target.value, setHotelName)} />
                                <TextField id="standard-basic" required label="Address" value={address} onChange={(event) => onChangeValue(event.target.value, setAddress)} />
                                <TextField id="standard-basic" type="number" label="Rating" value={rating} onChange={(event) => onChangeValue(event.target.value, setRating)} />
                                <TextField id="standard-basic" label="Phone" value={phone} onChange={(event) => onChangeValue(event.target.value, setPhone)} />
                                <TextField id="standard-basic" label="Email" value={email} onChange={(event) => onChangeValue(event.target.value, setEmail)} />
                                <TextField id="standard-basic" label="Country" value={country} onChange={(event) => onChangeValue(event.target.value, setCountry)} />
                                <TextField id="standard-basic" label="Pincode" value={pincode} onChange={(event) => onChangeValue(event.target.value, setPincode)} />
                                <TextField id="standard-basic" type="number" required label="Latitude" value={latitude} onChange={(event) => onChangeValue(event.target.value, setLatitude)} />
                                <TextField id="standard-basic" type="number" required label="Longitude" value={longitude} onChange={(event) => onChangeValue(event.target.value, setlongitude)} />
                                <TextField id="standard-basic" label="Check In Time" value={checkIn} onChange={(event) => onChangeValue(event.target.value, setCheckIn)} />
                                <TextField id="standard-basic" label="Check Out TIme" value={checkOut} onChange={(event) => onChangeValue(event.target.value, setCheckOut)} />
                            </FormGroup>
                            <Button variant="contained" color="primary" type="submit">Submit</Button>
                            <Button style={{ margin: "20px" }} variant="contained" color="secondary" onClick={goBack}>Go Back</Button>
                        </form>
                        {data && <p>{`Hotel with id ${data.addHotel.id} added`}</p>}
                        {error && <p>Error Ocuured</p>}
                    </Paper>
                </Grid>
            </Grid>
        </div>
    )
}

export default CreateHotel;
