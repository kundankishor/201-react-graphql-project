import { gql } from "@apollo/client";

export default gql`
query getHotelById($id:Int!){
    hotel(id:$id) {
      id
      name
      address
      rating
      reviews {
        reviewer
        comments
      }
      amenities {
        wifi
        swimmingPool
      }
    }
    }
`;