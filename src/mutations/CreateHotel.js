import { gql } from "@apollo/client";

export default gql`
    mutation AddHotel($hotel:HotelInput!){
        addHotel(hotel:$hotel){
            id
        }
    }
`;