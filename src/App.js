import './App.css';
import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client';
import HotelList from './components/HotelList';
import { Route, Switch } from 'react-router';
import HotelDetails from './components/HotelsDetails';
import CreateHotel from './components/CreateHotel';
import {
  Container,
} from '@material-ui/core';
import HotelLazyQuery from './components/HotelLazyQuery';

const client = new ApolloClient({
  uri: 'http://10.0.0.111:1111/graphql',
  cache: new InMemoryCache()
});


function App() {
  
  return (
    <ApolloProvider client={client}>
      <Container maxWidth="md">
        <Switch>
          <Route path="/" component={HotelList} exact />
          <Route path="/hotels/add" component={CreateHotel} />
          <Route path="/hotels/:id" component={HotelDetails} />
          <Route path="/lazyquery" component={HotelLazyQuery} />
        </Switch>
      </Container>
    </ApolloProvider>
  );
}

export default App;
