import { gql } from "@apollo/client";

export default gql`
query allHotels{
    hotels {
      id
      name
      address
      rating
    }
}
`;